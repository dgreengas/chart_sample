import 'package:chart_sample/pages/add_point_page.dart';
import 'package:chart_sample/providers/chart_data_provider.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ChartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Sample Chart"),
        centerTitle: true,
      ),
      body: Container(
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(18)),
          gradient: LinearGradient(
            colors: [
              Color(0xff2c274c),
              Color(0xff46426c),
            ],
            begin: Alignment.bottomCenter,
            end: Alignment.topCenter,
          ),
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              const SizedBox(
                height: 37,
              ),
              const Text(
                'Whoah Data',
                style: TextStyle(
                  color: Color(0xff827daa),
                  fontSize: 16,
                ),
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: 4,
              ),
              const Text(
                'Monthly Sales',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 32,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 2),
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: 37,
              ),
              Container(
                height: 100.0,
                child: Padding(
                  padding: const EdgeInsets.only(right: 16.0, left: 6.0),
                  child: Visibility(
                    visible: context.watch<ChartDataProvider>().data.length > 0,
                    child: LineChart(
                      LineChartData(
                          lineTouchData: LineTouchData(
                            touchTooltipData: LineTouchTooltipData(
                              tooltipBgColor: Colors.blueGrey.withOpacity(0.8),
                            ),
                            touchCallback: (LineTouchResponse touchResponse) {},
                            handleBuiltInTouches: true,
                          ),
                          gridData: FlGridData(
                            show: false,
                          ),
                          titlesData: FlTitlesData(
                            bottomTitles: SideTitles(
                              showTitles: true,
                              reservedSize: 22,
                              getTextStyles: (value) => const TextStyle(
                                color: Color(0xff72719b),
                                fontWeight: FontWeight.bold,
                                fontSize: 16,
                              ),
                              margin: 10,
                              getTitles: (value) {
                                switch (value.toInt()) {
                                  case 2:
                                    return 'SEPT';
                                  case 7:
                                    return 'OCT';
                                  case 12:
                                    return 'DEC';
                                }
                                return '';
                              },
                            ),
                            leftTitles: SideTitles(
                              showTitles: true,
                              getTextStyles: (value) => const TextStyle(
                                color: Color(0xff75729e),
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                              ),
                              getTitles: (value) {
                                switch (value.toInt()) {
                                  case 1:
                                    return '1m';
                                  case 2:
                                    return '2m';
                                  case 3:
                                    return '3m';
                                  case 4:
                                    return '5m';
                                }
                                return '';
                              },
                              margin: 8,
                              reservedSize: 30,
                            ),
                          ),
                          borderData: FlBorderData(
                            show: true,
                            border: const Border(
                              bottom: BorderSide(
                                color: Color(0xff4e4965),
                                width: 4,
                              ),
                              left: BorderSide(
                                color: Colors.transparent,
                              ),
                              right: BorderSide(
                                color: Colors.transparent,
                              ),
                              top: BorderSide(
                                color: Colors.transparent,
                              ),
                            ),
                          ),
                          minX: 0,
                          maxX: 14,
                          maxY: 4,
                          minY: 0,
                          lineBarsData: [
                            LineChartBarData(
                              spots: context
                                          .watch<ChartDataProvider>()
                                          .data
                                          .length >
                                      0
                                  ? context.watch<ChartDataProvider>().data
                                  : [FlSpot(0.0, 0.0)],
                            )
                          ]),
                      swapAnimationDuration: const Duration(milliseconds: 250),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              AddPointPage(),
            ],
          ),
        ),
      ),
    );
  }
}
