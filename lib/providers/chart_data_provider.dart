import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class ChartDataProvider with ChangeNotifier {
  List<FlSpot> _data = List<FlSpot>();

  List<FlSpot> get data =>
      List.unmodifiable(_data..sort((a, b) => a.x.compareTo(b.x)));

  void addPoint(FlSpot spot) {
    _data.add(spot);
    notifyListeners();
  }

  void removePoint(FlSpot spot) {
    _data.remove(spot);
    notifyListeners();
  }

  void removeAllPoints() {
    _data.clear();
    notifyListeners();
  }
}
