import 'package:chart_sample/providers/chart_data_provider.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class AddPointPage extends StatefulWidget {
  @override
  _AddPointPageState createState() => _AddPointPageState();
}

class _AddPointPageState extends State<AddPointPage> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _xController = TextEditingController();
  final TextEditingController _yController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    ChartDataProvider chartDataProvider = context.watch<ChartDataProvider>();
    return Container(
      color: Colors.red[200],
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            TextFormField(
              controller: _xController,
              decoration: InputDecoration(
                helperText: "X-Coordinate",
                icon: Icon(Icons.calculate),
              ),
              keyboardType: TextInputType.number,
              validator: _validateIsDouble,
            ),
            TextFormField(
              controller: _yController,
              decoration: InputDecoration(
                  helperText: "Y-Coordinate", icon: Icon(Icons.calculate)),
              keyboardType: TextInputType.number,
              validator: _validateIsDouble,
            ),
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              ElevatedButton.icon(
                icon: Icon(Icons.add),
                onPressed: () {
                  if (_formKey.currentState.validate()) {
                    chartDataProvider.addPoint(FlSpot(
                        double.parse(_xController.text),
                        double.parse(_yController.text)));
                  }
                },
                label: Text("Add"),
              ),
              ElevatedButton.icon(
                icon: Icon(Icons.delete),
                onPressed: () {
                  if (_formKey.currentState.validate()) {
                    chartDataProvider.removePoint(FlSpot(
                        double.parse(_xController.text),
                        double.parse(_yController.text)));
                  }
                },
                label: Text("Remove"),
              ),
              ElevatedButton.icon(
                icon: Icon(Icons.delete_forever),
                onPressed: () {
                  chartDataProvider.removeAllPoints();
                },
                label: Text("Reset"),
              ),
            ])
          ],
        ),
      ),
    );
  }

  String _validateIsDouble(String value) {
    RegExp isDouble = RegExp("^(-?)(0|([1-9][0-9]*))(\\.[0-9]+)?\$");
    if (isDouble.hasMatch(value)) {
      return null;
    } else {
      return "Enter a valid number";
    }
  }
}
